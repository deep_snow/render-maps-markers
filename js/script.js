'use strict';

(function () {
  var URL_LOAD = 'https://catalog.api.2gis.ru/2.0/catalog/marker/';
  var OK_STATUS = 200;
  var PAGE_SIZE = 15000;
  var REGION_ID = 32;
  var KEY = 'ruhebf8058';
  var MARKER_WIDTH = 22;
  var MARKER_HEIGHT = 34;

  var searchForm = document.forms.searchForm;
  // var searchForm = document.querySelector('form[name=searchForm]');
  var searchIcon = searchForm.querySelector('.search__icon');

  var errorShown;

  var loadedData;

  var map;
  var layerMarkers;
  var visibleMapCoord = {};


  DG.then(function () {
    layerMarkers = DG.featureGroup();
    map = DG.map('map', {
      center: [55.75, 37.62],
      zoom: 10,
      zoomControl: false
    });

    DG.control.zoom({position: 'topright'}).addTo(map);

    visibleMapCoord.northWest = map.getBounds().getNorthWest();
    visibleMapCoord.southEast = map.getBounds().getSouthEast();

    map.on('moveend', onMoveMap);
  });


  /**
   * Обращение к API по URL с целью поиска по заданному слову.
   * @param {String} searchWord - Значение поля поиска
   * @param {function} onSuccess - Выполняется в случае успешно выполненого запроса.
   */
  function getSearchResult(searchWord, onSuccess) {
    var searchXhr = new XMLHttpRequest();
    searchXhr.responseType = 'json';

    searchXhr.addEventListener('load', function () {
      if (searchXhr.readyState === 4 && searchXhr.status === OK_STATUS) {
        onSuccess(searchXhr.response);
      } else {
        showError(searchXhr.statusText);
      }
      searchIcon.classList.toggle('search__icon--load', false);
    });

    searchXhr.open('GET', [URL_LOAD, 'search?q=', searchWord, '&page_size=', PAGE_SIZE, '&region_id=', REGION_ID, '&key=', KEY].join(''));
    searchXhr.send(null);
  }


  function showError(msg) {
    hideError();
    var errorBox = document.createElement('div');
    errorBox.classList.add('error-box');
    errorBox.textContent = msg || 'Произошла ошибка.';
    searchForm.insertAdjacentElement('afterend', errorBox);
    errorShown = errorBox;
    errorBox.addEventListener('click', hideError);
  }


  function hideError() {
    if (errorShown) {
      errorShown.remove();
    }
  }


  /**
   * Фильтрация данных относящихся к видимой части карты.
   * @param {Array} inputData Входящий массив данных
   * @return {Array} Новый массив содержащий только данные находящиеся в видимой части карты
   */
  function filterDataOnVisibleMap(inputData) {
    var filterData = inputData.filter(function (item) {
      return ((item.lat <= visibleMapCoord.northWest.lat) && (item.lat >= visibleMapCoord.southEast.lat)) && ((item.lon >= visibleMapCoord.northWest.lng) && (item.lon <= visibleMapCoord.southEast.lng));
    });
    return filterData;
  }


  /**
   * Получение массива заданной длины.
   * @param {array} array - Исходный массив
   * @param {length} length - Количество элементов
   * @return {Array} Новый массив
   */
  function getPartArray(array, length) {
    return array.slice(0, length);
  }


  /**
  * Сортировка массива объектов по их свойствам.
  * Взято из статьи "Несколько полезных кейсов при работе с массивами в JavaScript" (habr.com/post/279867/)
  * @param {string} field - Свойство по которому проходит сортировка
  * @param {number} order - Порядок сортировки
  * @return {array} Отсортированный массив
  */
  function compare(field, order) {
    var len = arguments.length;
    if (len === 0) {
      return (a, b) => (a < b && -1) || (a > b && 1) || 0;
    }
    if (len === 1) {
      switch (typeof field) {
        case 'number':
          return field < 0 ?
            ((a, b) => (a < b && 1) || (a > b && -1) || 0) :
            ((a, b) => (a < b && -1) || (a > b && 1) || 0);
        case 'string':
          return (a, b) => (a[field] < b[field] && -1) || (a[field] > b[field] && 1) || 0;
      }
    }
    if (len === 2 && typeof order === 'number') {
      return order < 0 ?
        ((a, b) => (a[field] < b[field] && 1) || (a[field] > b[field] && -1) || 0) :
        ((a, b) => (a[field] < b[field] && -1) || (a[field] > b[field] && 1) || 0);
    }
    var fields, orders;
    if (typeof field === 'object') {
      fields = Object.getOwnPropertyNames(field);
      orders = fields.map(key => field[key]);
      len = fields.length;
    } else {
      fields = new Array(len);
      orders = new Array(len);
      for (var i = len; i--;) {
        fields[i] = arguments[i];
        orders[i] = 1;
      }
    }
    return (a, b) => {
      for (var i = 0; i < len; i++) {
        if (a[fields[i]] < b[fields[i]]) return orders[i];
        if (a[fields[i]] > b[fields[i]]) return -orders[i];
      }
      return 0;
    };
  }


  /**
   * Сортировка массива по свойству longitude (долгота) по убыванию.
   * @param {Array} inputData - Исходный массив
   * @return {Array} Отсортированный массив
   */
  function sortDataOnLon(inputData) {
    return inputData.sort(compare('lon', -1));
  }


  function renderMarkers(inputData) {
    inputData.forEach(function (item) {
      DG.marker([item.lat, item.lon]).addTo(layerMarkers);
    });
    layerMarkers.addTo(map);
  }


  function deleteMarkers() {
    layerMarkers.clearLayers();
    layerMarkers.removeFrom(map);
  }


  /**
   * Получение значений смещения из свойства transform для указанного маркера.
   * @param {Node} marker - Маркер.
   * @return {Object} Смещение маркера в числовом значении
   */
  function getTransformMarker(marker) {
    var styleTransformMarker = marker.style.transform;
    var transformMarker = styleTransformMarker.substring(styleTransformMarker.indexOf('(') + 1, styleTransformMarker.indexOf(')'));

    var transformMarkerX = parseFloat(transformMarker.substring(0, transformMarker.indexOf(',')));
    var transformMarkerY = parseFloat(transformMarker.substring(transformMarker.indexOf(',') + 1, transformMarker.indexOf(' 0px')));
    return {
      x: transformMarkerX,
      y: transformMarkerY
    };
  }


  function hideMarkers() {
    var listMarkers = layerMarkers.getLayers();

    for (var i = 0; i < listMarkers.length; i++) {
      var marker = listMarkers[i].getElement();

      if (marker.style.display !== 'none') {
        var markerTransform = getTransformMarker(marker);

        for (var j = i + 1; j < listMarkers.length; j++) {
          var markerNext = listMarkers[j].getElement();
          var markerNextTransform = getTransformMarker(markerNext);
          if (markerNext.style.display !== 'none') {
            if (Math.abs(markerTransform.x - markerNextTransform.x) < MARKER_WIDTH) {
              if (Math.abs(markerTransform.y - markerNextTransform.y) < MARKER_HEIGHT) {
                markerNext.style.display = 'none';
              } else {
                continue;
              }
            } else {
              break;
            }
          }
        }
      }
    }
  }


  function onLoadSearchResult(loadData) {
    if (loadData.meta.code === OK_STATUS) {
      loadedData = loadData.result.items;
      var filteredData = getPartArray(filterDataOnVisibleMap(loadData.result.items), 1000);
      renderMarkers(sortDataOnLon(filteredData));
      hideMarkers();
    } else {
      showError('Ничего не найдено.');
    }
  }


  function onMoveMap() {
    visibleMapCoord.northWest = map.getBounds().getNorthWest();
    visibleMapCoord.southEast = map.getBounds().getSouthEast();
    if (loadedData) {
      deleteMarkers();
      var filteredData = getPartArray(filterDataOnVisibleMap(loadedData), 1000);
      renderMarkers(sortDataOnLon(filteredData));
      hideMarkers();
    }
  }


  searchForm.addEventListener('submit', function (evt) {
    evt.preventDefault();
    searchIcon.classList.toggle('search__icon--load', true);
    hideError();
    deleteMarkers();
    getSearchResult(searchForm.search.value, onLoadSearchResult);
  });

})();
